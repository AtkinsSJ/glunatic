GLunatic

An attempt to port the original Dr Lunatic to SDL, with OpenGL rendering.

If this ever works, what you will need to do is build the executable, then put it in the same folder as the original lunatic.exe - we can't include the game assets (fair enough) so you'll need to own and install Dr Lunatic!

I will very likely get distracted and not finish this, but it's worth a try!