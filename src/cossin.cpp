#include "cossin.h"
#include <math.h>

#define DEGREES  (256)

int costab[DEGREES];
int sintab[DEGREES];

void InitCosSin(void)
{
	int i;

	for(i=0;i<DEGREES;i++)
	{
		costab[i]=(int)(cos((float)i*3.14159*2/DEGREES)*FIXAMT);
		sintab[i]=(int)(sin((float)i*3.14159*2/DEGREES)*FIXAMT);
	}
}

int Cosine(int angle)
{
	return costab[angle];
}

int Sine(int angle)
{
	return sintab[angle];
}

void Dampen(int *value,int amt)
{
	if(*value>0)
	{
		*value-=amt;
		if(*value<0)
			*value=0;
	}
	if(*value<0)
	{
		*value+=amt;
		if(*value>0)
			*value=0;
	}
}

void Clamp(int *value,int amt)
{
	if(*value>amt)
		*value=amt;
	if(*value<-amt)
		*value=-amt;
}

Matrix4 orthographicMatrix4(f32 left, f32 right, f32 top, f32 bottom, f32 nearClip, f32 farClip) {
	Matrix4 m = {};
	m.v[0][0] = 2.0f / (right-left);
	m.v[1][1] = 2.0f / (top-bottom);
	m.v[2][2] = -2.0f / (farClip-nearClip);

	m.v[3][0] = -(right+left) / (right-left);
	m.v[3][1] = -(top+bottom) / (top-bottom);
	m.v[3][2] = -(farClip+nearClip) / (farClip-nearClip);
	m.v[3][3] = 1.0f;

	return m;
}