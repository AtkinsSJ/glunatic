/* DR. LUNATIC (working title)
   
   A HamumuSoft Production.

   v 0.04

   Copyright 1998, Mike Hommel
*/

#include "winpch.h"
#include "sdldraw.h"
#include "jamulfont.h"
#include "jamulsound.h"
#include <shellapi.h>
//#include <crtdbg.h>

#include "game.h"
#include "editor.h"
#include "tile.h"
#include "sound.h"
#include "monster.h"
#include "title.h"

struct Application_Setup
{
	bool windowed;
	int width;
	int height;
};

Application_Setup DefaultApplicationSetup()
{
	Application_Setup setup = {};
	setup.windowed = false;
	setup.width = 640;
	setup.height = 480;

	return setup;
}

void ParseCmdLine(int argCount, char *args[], Application_Setup *result)
{
	for (int argIndex=0; argIndex < argCount; argIndex++)
	{
		// "window" is the only command line switch handled by dr lunatic!
		// But we probably want to add more later.
		if (equals(args[argIndex], "window"))
		{
			result->windowed = true;
		}
	}
}

int main(int argCount, char *args[])
{
	Application_Setup appSetup = DefaultApplicationSetup();
	ParseCmdLine(argCount, args, &appSetup);

	SDLDraw *renderer = new SDLDraw("GLunatic Alpha", appSetup.width, appSetup.height, appSetup.windowed);

	if (!renderer)
	{
		logCritical("Failed to create a renderer!");
		return 1;
	}

	LunaticInit(renderer);
	SplashScreen(renderer, "graphics\\hamumu.bmp", 128, 2);

	while(1)
	{
		switch(MainMenu(renderer))
		{
			case 255:	// quit
#ifdef DEMO
				DemoSplashScreens(renderer);
#endif
				LunaticExit();
				delete renderer;
				return 0;
				break;
			case 0:	// new game
				LunaticGame(renderer,0);
				break;
			case 1:	// continue
				LunaticGame(renderer,1);
				break;
			case 3:	// editor
				LunaticEditor(renderer);
				break;
			case 4:	// ordering
				LunaticExit();
				delete renderer;
				ShellExecute(NULL,"open","docs\\order.html","","",SW_SHOWNORMAL);
				return 0;
				break;
		}
	}

	return 0;
}

