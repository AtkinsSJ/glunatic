#ifndef RAGE_H
#define RAGE_H

#include "winpch.h"
#include "display.h"
#include "guy.h"

void ShowRage(SDLDraw *mgl);
byte UpdateRage(SDLDraw *mgl);
void StartRaging(void);
void DoRage(Guy *me);

#endif