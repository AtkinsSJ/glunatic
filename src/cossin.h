#ifndef COSSIN_H
#define COSSIN_H

#include "winpch.h"

void InitCosSin(void);
int Cosine(int angle);
int Sine(int angle);
void Dampen(int *value,int amt);
void Clamp(int *value,int amt);

// Let's be honest, this is a general-purpose maths file, not cos/sin specific.
// So, I'm dumping other mathsy stuff here!

struct Matrix4 {
	union {
		f32 v[4][4]; // Column-major order, so [COLUMN][ROW]
		f32 flat[4*4];
	};
};

Matrix4 orthographicMatrix4(f32 left, f32 right, f32 top, f32 bottom, f32 nearClip, f32 farClip);

#endif