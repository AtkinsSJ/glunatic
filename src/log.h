#define logVerbose(...) SDL_LogError(SDL_LOG_CATEGORY_CUSTOM, __VA_ARGS__)
#define logDebug(...) SDL_LogDebug(SDL_LOG_CATEGORY_CUSTOM, __VA_ARGS__)
#define logInfo(...) SDL_LogInfo(SDL_LOG_CATEGORY_CUSTOM, __VA_ARGS__)
#define logWarn(...) SDL_LogWarn(SDL_LOG_CATEGORY_CUSTOM, __VA_ARGS__)
#define logError(...) SDL_LogError(SDL_LOG_CATEGORY_CUSTOM, __VA_ARGS__)
#define logCritical(...) SDL_LogCritical(SDL_LOG_CATEGORY_CUSTOM, __VA_ARGS__)