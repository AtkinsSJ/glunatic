/* these are basic types I just can't live without */

#ifndef JAMTYPES_H
#define JAMTYPES_H

// I feel horrible doing this, but this is a dumb warning.
// it warns when the compiler is generating overhead by converting the value to
// a bool type, which in C++ means it actually forces it to be either 0 or 1, which
// requires extra code.

#ifndef __MINGW32__     // appears to be a Visual Studio thing
#pragma warning( disable : 4800 )
#endif

#define FIXSHIFT (16)
#define FIXAMT   (65536)

typedef unsigned char	byte;
typedef unsigned short	word;
typedef unsigned long	dword;

#define TRUE 1
#define FALSE 0

// Non-jamul stuff below!
typedef float f32;
typedef double f64;

#define GLUE_(a, b) a ## b
#define GLUE(a, b) GLUE_(a, b)
#define STRVAL_(a) #a
#define STRVAL(a) STRVAL_(a)

/*
This macro lets you run a block of code at the end of the current scope. Just do:
	DEFER(code_goes_here(foo, 123, true, whatever));
and it will work! Can be any number of statements. If you defer multiple blocks within the
same scope, they will execute in LIFO order, just like destructors do (because it uses
destructors!
*/
#include <functional>
#define DEFER_STRUCT_NAME GLUE(defer_, __LINE__)
#define DEFER(the_code)                                                     \
	struct DEFER_STRUCT_NAME {                                              \
		std::function<void ()> deferredCode;                                \
		DEFER_STRUCT_NAME(std::function<void ()> deferredCode){             \
			this->deferredCode = deferredCode;                              \
		}                                                                   \
		~DEFER_STRUCT_NAME(){                                               \
			this->deferredCode();                                           \
		}                                                                   \
	};                                                                      \
	DEFER_STRUCT_NAME GLUE(_, DEFER_STRUCT_NAME)( [&](){the_code;} );

#define assert SDL_assert

inline bool equals(char *a, char *b)
{
	return (strcmp(a,b) == 0);
}

struct V2 {
	f32 x,y;
};
inline V2 v2(f32 x, f32 y)
{
	V2 result;

	result.x = x;
	result.y = y;

	return result;
}

struct V3 {
	union {
		struct {
			f32 x,y,z;
		};
		struct {
			V2 xy;
			f32 z;
		};
	};
};
inline V3 v3(f32 x, f32 y, f32 z)
{
	V3 result;

	result.x = x;
	result.y = y;
	result.z = z;

	return result;
}

struct V4 {
	union {
		struct {
			V3 xyz;
			f32 w;
		};
		struct {
			V2 xy;
			f32 z, w;
		};
		struct {
			f32 x,y,z,w;
		};
		struct {
			f32 r,g,b,a;
		};
	};
};
inline V4 v4(f32 r, f32 g, f32 b, f32 a)
{
	V4 result;

	result.r = r;
	result.g = g;
	result.b = b;
	result.a = a;

	return result;
}

#endif