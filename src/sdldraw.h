#ifndef SDLDRAW_H
#define SDLDRAW_H

#include "winpch.h"
#include "jamulsound.h"
#include "control.h"
#include <stdio.h>

#include <SDL.h>
#include <SDL_image.h>
#include <gl/glew.h>
#include <SDL_opengl.h>

// For appdata storage of stuff
FILE* AppdataOpen(const char* filename, const char* mode);

// Replacement for missing palette_t
typedef struct palette_t {
    byte red, green, blue, alpha;
} palette_t;

// Replacement for missing MGL functions
int MGL_random(int max);
void MGL_srand(int seed);
long MGL_randoml(long max);
void MGL_fatalError(const char* txt);

struct ShaderProgram
{
	GLuint id;

	GLint uProjectionMatrix;
	GLint uTexture;
	GLint uPalette;

	GLint aPosition;
	GLint aColor;
	GLint aUV;
};

struct StandardVertex
{
	V3 position;
	V4 color;
	V2 uv;

	StandardVertex() {};
	StandardVertex(V3 position, V4 color, V2 uv);
};

void checkForGLError();

class SDLDraw
{
	private:
		bool readyToQuit;

		SDL_Window *window;
		int width;
		int height;
		palette_t pal[256];

		SDL_GLContext context;
		GLuint VBO;
		GLuint IBO;
		ShaderProgram standardShader;

		GLuint textureID;
		GLuint paletteID;
		
		dword elapsedTime;
		dword now;
		int numFrames;
		char lastKeyPressed;
        int mousex,mousey;
		byte mouseDown;

	public:
		SDLDraw(const char *name,int xRes,int yRes,bool window);
		~SDLDraw();

		void FatalError(const char *msg1, const char *msg2=NULL);

		bool Process(void);	// handle windows messages and such
		
	// I think all of this little group need rethinking
		// HWND GetHWnd(void);
		byte *GetScreen(void); // get a pointer to the screen memory
		int GetWidth(void);
		int GetHeight(void);
		void ClearScreen(void);
		void Flip(void);
		void Quit(void);
	// }

		bool LoadPalette(char *name);
		void SetPalette(palette_t *pal2);
		void RealizePalette(void);
		void CopyPaletteTo(palette_t *out);

		// Apparently this loads and immediately draws the bitmap to the screen
		// For ref:  we load the bmp into TEXTURE0, and the palette TEXTURE1
		// Also stores the loaded palette in this->pal
		bool LoadBMP(const char *name);

		char LastKeyPressed(void);
		char LastKeyPeek(void);
		void SetLastKey(char c);

		void GammaCorrect(byte gamma);

		// handy little drawing routines
		void Box(int x,int y,int x2,int y2,byte c);
		void FillBox(int x,int y,int x2,int y2,byte c);
		
		// functions to measure frame rate
		void ResetTimer(void);
		void TickTimer(void);
		float FrameRate(void);

		// mouse functions
		byte MouseDown(void);
		void SetMouseDown(byte w);
        void SetMouse(int x, int y);
        void TeleportMouse(int x, int y);
        void GetMouse(int *x, int *y);
};

#endif