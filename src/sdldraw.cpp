
#include "sdldraw.h"
#include "winpch.h"
#include "game.h"	// for SetGameIdle and GetGameIdle only
#include "sound.h"
#include "music.h"
#include "twister.h"
#include "shlobj.h" // for SHGetFolderPath
#include <stdio.h>
#include <direct.h> // for _mkdir
#include "cossin.h"

// Appdata shenanigans
FILE* AppdataOpen(const char* file, const char* mode) {
    char buffer[MAX_PATH];
    SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, buffer);
    sprintf(buffer + strlen(buffer), "\\Hamumu");
    _mkdir(buffer);
    sprintf(buffer + strlen(buffer), "\\DrLunatic");
    _mkdir(buffer);
    sprintf(buffer + strlen(buffer), "\\%s", file);
    return fopen(buffer, mode);
}

// Allegro shenanigans
// static char prevKey[KEY_MAX];
// static bool closeButtonPressed;
// void closeButtonCallback() { closeButtonPressed = true; }
// void switchInCallback() { SetGameIdle(0); }
// void switchOutCallback() { SetGameIdle(1); }
MTRand mtRand;

// Replacements for missing MGL functions
int MGL_random(int max) {
    return mtRand.randInt(max - 1);
}
void MGL_srand(int seed) {
    mtRand.seed(seed);
}
long MGL_randoml(long max) {
    return mtRand.randInt(max - 1);
}
void MGL_fatalError(const char* txt) {
	SDL_LogCritical(SDL_LOG_CATEGORY_CUSTOM, txt);
	exit(0);
}

void checkForGLError()
{
	GLenum errorCode = glGetError();
	if (errorCode != GL_NO_ERROR)
	{
		logError("GL error %d: %s", errorCode, gluErrorString(errorCode));
		assert(false);
	}
}

StandardVertex::StandardVertex(V3 position, V4 color, V2 uv)
{
	this->position = position;
	this->color = color;
	this->uv = uv;
}

ShaderProgram CreateStandardShader()
{
	const char *standardShaderVertex = R"END(
#version 150
in vec3 aPosition;
in vec4 aColor;
in vec2 aUV;

out vec4 vColor;
out vec2 vUV;

uniform mat4 uProjectionMatrix;

void main() {
	gl_Position = uProjectionMatrix * vec4( aPosition.xyz, 1 );
	vColor = aColor;
	vUV = aUV;
}
)END";
	const char *standardShaderFragment = R"END(
#version 150

uniform sampler2D uTexture;
uniform sampler1D uPalette;

in vec4 vColor;
in vec2 vUV;

out vec4 fragColor;

void main() {
	int colorIndex = int(255.0 * texture(uTexture, vUV).r);
	vec4 texel = texelFetch(uPalette, colorIndex, 0);

	fragColor = vColor;
	fragColor *= texel;
}
)END";

	ShaderProgram result = {};

	result.id = glCreateProgram();
	if (!result.id)
	{
		logError("Failed to create a shader program");
	}
	else
	{
		// Vertex
		{
			GLuint vShader = glCreateShader(GL_VERTEX_SHADER);
			DEFER(glDeleteShader(vShader));

			glShaderSource(vShader, 1, &standardShaderVertex, NULL);
			glCompileShader(vShader);
			
			GLint isCompiled = GL_FALSE;
			glGetShaderiv(vShader, GL_COMPILE_STATUS, &isCompiled);
			if (isCompiled == GL_TRUE)
			{
				glAttachShader(result.id, vShader);
			}
			else
			{
				// Print a big error message about this!
				int logMaxLength = 0;
				glGetShaderiv(vShader, GL_INFO_LOG_LENGTH, &logMaxLength);
				char *infoLog = new char[logMaxLength];
				DEFER(delete[] infoLog);

				int logLength = 0;
				glGetShaderInfoLog(vShader, logMaxLength, &logLength, infoLog);

				if (logLength == 0)
				{
					infoLog = "No error log provided by OpenGL. Sad panda.";
				}

				logError("Unable to compile shader %d, \'standard vertex\'! (%s)", vShader, infoLog);
			}
		}

		// Fragment
		{
			GLuint fShader = glCreateShader(GL_FRAGMENT_SHADER);
			DEFER(glDeleteShader(fShader));

			glShaderSource(fShader, 1, &standardShaderFragment, NULL);
			glCompileShader(fShader);
			
			GLint isCompiled = GL_FALSE;
			glGetShaderiv(fShader, GL_COMPILE_STATUS, &isCompiled);
			if (isCompiled == GL_TRUE)
			{
				glAttachShader(result.id, fShader);
			}
			else
			{
				// Print a big error message about this!
				int logMaxLength = 0;
				glGetShaderiv(fShader, GL_INFO_LOG_LENGTH, &logMaxLength);
				char *infoLog = new char[logMaxLength];
				DEFER(delete[] infoLog);

				int logLength = 0;
				glGetShaderInfoLog(fShader, logMaxLength, &logLength, infoLog);

				if (logLength == 0)
				{
					infoLog = "No error log provided by OpenGL. Sad panda.";
				}

				logError("Unable to compile shader %d, \'standard fragment\'! (%s)", fShader, infoLog);
			}
		}

		glLinkProgram(result.id);

		GLint programSuccess = GL_FALSE;
		glGetProgramiv(result.id, GL_LINK_STATUS, &programSuccess);

		if (programSuccess != GL_TRUE)
		{
			// Print a big error message about this!
			int logMaxLength = 0;
			glGetShaderiv(result.id, GL_INFO_LOG_LENGTH, &logMaxLength);
			char *infoLog = new char[logMaxLength];
			DEFER(delete[] infoLog);

			int logLength = 0;
			glGetProgramInfoLog(result.id, logMaxLength, &logLength, infoLog);

			if (logLength == 0)
			{
				infoLog = "No error log provided by OpenGL. Sad panda.";
			}

			logError("Unable to compile shader program %d, \'standard\'! (%s)", result.id, infoLog);
		}
		else
		{
			result.uProjectionMatrix = glGetUniformLocation(result.id, "uProjectionMatrix");
			result.uTexture          = glGetUniformLocation(result.id, "uTexture");
			result.uPalette          = glGetUniformLocation(result.id, "uPalette");

			result.aPosition = glGetAttribLocation(result.id, "aPosition");
			result.aColor    = glGetAttribLocation(result.id, "aColor");
			result.aUV       = glGetAttribLocation(result.id, "aUV");
		}
	}

	return result;
}

SDLDraw::SDLDraw(const char *name, int width, int height, bool windowed)
{
	this->readyToQuit = false;
	this->width = width;
	this->height = height;

	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);

 	// TODO: More subsystems as we need them!
	if (SDL_Init(SDL_INIT_VIDEO)) // returns non-zero on error! Because sure, false is a great value for success.
	{
		FatalError("Failed to initialize SDL! That is quite bad.", SDL_GetError());
	}
	
	dword windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
	if (!windowed)
	{
		windowFlags |= SDL_WINDOW_FULLSCREEN;
	}
	this->window = SDL_CreateWindow(name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, windowFlags);

	if (!this->window)
	{
		FatalError("We couldn't create a window! MEGA BAD.", SDL_GetError());
	}

	// Use GL3.1 Core
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// Create context
	this->context = SDL_GL_CreateContext(window);
	if (this->context == NULL)
	{
		FatalError("OpenGL context could not be created! :(", SDL_GetError());
	}

	// GLEW
	/*
		"GLEW obtains information on the supported extensions from the graphics driver. Experimental or pre-release drivers, however, might not report every available extension through the standard mechanism, in which case GLEW will report it unsupported. To circumvent this situation, the glewExperimental global switch can be turned on by setting it to GL_TRUE before calling glewInit(), which ensures that all extensions with valid entry points will be exposed."
		http://glew.sourceforge.net/basic.html
	*/
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();
	if (glewError != GLEW_OK)
	{
		FatalError("Could not initialise GLEW! :(", (char*)glewGetErrorString(glewError));
	}

	// VSync
	if (SDL_GL_SetSwapInterval(1) < 0)
	{
		logError("Could not set vsync! :(", SDL_GetError());
		// succeeded = false; // Don't need to crash just because no v-sync!
	}

	glViewport(0, 0, this->width, this->height);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);

	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->IBO);

	glGenTextures(1, &this->textureID);
	glGenTextures(1, &this->paletteID);

	// Shaders
	this->standardShader = CreateStandardShader();

	GLenum errorCode = glGetError();
	if (errorCode != 0)
	{
		logDebug("GL Error %d: %s", errorCode, gluErrorString(errorCode));
	}
}

SDLDraw::~SDLDraw()
{
	SDL_GL_DeleteContext(this->context);
	SDL_Quit();
}

void SDLDraw::FatalError(const char *msg1, const char *msg2)
{
	logCritical("%s", msg1);
	if (msg2) logCritical("%s", msg2);

	exit(1);
}

bool SDLDraw::Process(void)	// handle windows messages and such
{
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
			// WINDOW EVENTS
			case SDL_QUIT: {
				this->readyToQuit = true;
			} break;

			case SDL_KEYDOWN: {
				SDL_Scancode sc = event.key.keysym.scancode;
				ControlKeyDown(sc);
				SetLastKey(sc & 0xff); // ? is this correct?
			} break;
			case SDL_KEYUP: {
				SDL_Scancode sc = event.key.keysym.scancode;
				ControlKeyUp(sc);
			} break;
		}
	}

	int mx, my;
	int mouseState = SDL_GetMouseState(&mx, &my);
	SetMouse(mx, my);
	SetMouseDown(mouseState & SDL_BUTTON(SDL_BUTTON_LEFT));

	return (!this->readyToQuit);
}

// HWND SDLDraw::GetHWnd(void)
// {

// }

byte *SDLDraw::GetScreen(void) // get a pointer to the screen memory
{
	assert(!"Deprecated, no direct rendering!");
	return 0;
}

int SDLDraw::GetWidth(void)
{
	return this->width;
}

int SDLDraw::GetHeight(void)
{
	return this->height;
}

void SDLDraw::ClearScreen(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
}

void SDLDraw::Flip(void)
{
	checkForGLError();

	if (GetGameIdle())
	{
		GameIdle();
	}

	{
		ShaderProgram *shader = &this->standardShader;
		glUseProgram(shader->id);
		Matrix4 projectionMatrix = orthographicMatrix4(0, this->width, 0, this->height, -1000, 1000);
		glUniformMatrix4fv(shader->uProjectionMatrix, 1, false, projectionMatrix.flat);

		glUniform1i(shader->uTexture, 0);
		glUniform1i(shader->uPalette, 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->textureID);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_1D, this->paletteID);

		// Vertices
		V4 color = v4(1,1,1,1);
		StandardVertex vertices[4];
		vertices[0] = StandardVertex(v3(0, 0, 0), color, v2(0,0));
		vertices[1] = StandardVertex(v3(width, 0, 0), color, v2(1,0));
		vertices[2] = StandardVertex(v3(width, height, 0), color, v2(1,1));
		vertices[3] = StandardVertex(v3(0, height, 0), color, v2(0,1));

		GLuint indices[6] = {0, 1, 2, 0, 2, 3};

		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
		glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(StandardVertex), vertices, GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), indices, GL_STREAM_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

		glEnableVertexAttribArray(shader->aPosition);
		glEnableVertexAttribArray(shader->aColor);
		glEnableVertexAttribArray(shader->aUV);

		glVertexAttribPointer(shader->aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(StandardVertex), (void*)offsetof(StandardVertex, position));
		glVertexAttribPointer(shader->aColor, 4, GL_FLOAT, GL_FALSE, sizeof(StandardVertex), (void*)offsetof(StandardVertex, color));
		glVertexAttribPointer(shader->aUV, 2, GL_FLOAT, GL_FALSE, sizeof(StandardVertex), (void*)offsetof(StandardVertex, uv));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->IBO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);

		glDisableVertexAttribArray(shader->aPosition);
		glDisableVertexAttribArray(shader->aColor);
		glDisableVertexAttribArray(shader->aUV);
	}

	SDL_GL_SwapWindow(this->window);
}

void SDLDraw::Quit(void)
{
	this->readyToQuit = true;
}

typedef struct palfile_t
{
	char r,g,b;
} palfile_t;

bool SDLDraw::LoadPalette(char *name)
{
	SDL_RWops *file;
	palfile_t p[256];
	int i;

	file = SDL_RWFromFile(name, "rb");
	if (!file)
		return false;

	if (SDL_RWread(file, p, sizeof(palfile_t), 256) != 256)
	{
		SDL_RWclose(file);
		return false;
	}

	for(i=0;i<256;i++)
	{
		pal[i].red=p[i].r;
		pal[i].green=p[i].g;
		pal[i].blue=p[i].b;
		pal[i].alpha=0;
	}

	RealizePalette();

	SDL_RWclose(file);
	return true;
}

void SDLDraw::SetPalette(palette_t *pal)
{
	glBindTexture(GL_TEXTURE_1D, paletteID);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA8, 256, 0, GL_RGBA, GL_UNSIGNED_BYTE, pal);
}

void SDLDraw::RealizePalette(void)
{
	// Original does nothing. Maybe we do nothing too?
}

void SDLDraw::CopyPaletteTo(palette_t *out)
{
	memcpy(out, this->pal, 256 * sizeof(palette_t));
}

bool SDLDraw::LoadBMP(const char *name)
{
	SDL_Surface *bmpSurface = SDL_LoadBMP(name);
	DEFER(SDL_FreeSurface(bmpSurface));

	if (!bmpSurface)
	{
		logError("Failed to load bitmap image '%s'", name);
		return false;
	}

 	// We only hadle 8bit palette bmps right now, like the original
	assert(bmpSurface->format->palette);

	// Texture
	glBindTexture(GL_TEXTURE_2D, this->textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, bmpSurface->w, bmpSurface->h, 0, GL_RED, GL_UNSIGNED_BYTE, bmpSurface->pixels);

	// Palette texture
	memcpy(this->pal, bmpSurface->format->palette->colors, 256 * sizeof(palette_t));
	
	return true;
}

void SDLDraw::SetLastKey(char c)
{
	this->lastKeyPressed = c;
}
char SDLDraw::LastKeyPressed(void)
{
	char c = this->lastKeyPressed;
	this->lastKeyPressed = 0;
	return c;
}
char SDLDraw::LastKeyPeek(void)
{
	return this->lastKeyPressed;
}

void SDLDraw::GammaCorrect(byte gamma)
{
	int i;
	int r,g,b;
	palette_t temp[256];

	memcpy(temp,pal,sizeof(palette_t)*256);
	for(i=0;i<256;i++)
	{
		r=pal[i].red;
		g=pal[i].green;
		b=pal[i].blue;
		r=(r*(gamma+4))/4;
		g=(g*(gamma+4))/4;
		b=(b*(gamma+4))/4;
		if(r>255)
			r=255;
		if(g>255)
			g=255;
		if(b>255)
			b=255;
		pal[i].red=r;
		pal[i].green=g;
		pal[i].blue=b;
	}
	this->RealizePalette();
	memcpy(pal,temp,sizeof(palette_t)*256);
}

// handy little drawing routines
void SDLDraw::Box(int x,int y,int x2,int y2,byte c)
{

}
void SDLDraw::FillBox(int x,int y,int x2,int y2,byte c)
{

}

void SDLDraw::ResetTimer(void)
{
	elapsedTime=0;
	now=timeGetTime();
	numFrames=0;
}
void SDLDraw::TickTimer(void)
{
	dword then;

	then=now;
	now=timeGetTime();
	elapsedTime+=(now-then);
	numFrames++;
}
float SDLDraw::FrameRate(void)
{
	return (float)(((float)numFrames*1000)/(float)elapsedTime);
}

void SDLDraw::SetMouseDown(byte w)
{
	this->mouseDown = w;
}
byte SDLDraw::MouseDown(void)
{
	return this->mouseDown;
}

void SDLDraw::SetMouse(int x, int y)
{
	this->mousex = x;
	this->mousey = y;
}
void SDLDraw::TeleportMouse(int x, int y)
{
	SDL_WarpMouseInWindow(this->window, x, y);
	SetMouse(x, y);
}
void SDLDraw::GetMouse(int *x, int *y)
{
	*x = this->mousex;
	*y = this->mousey;
}
