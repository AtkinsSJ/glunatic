#ifndef TITLE_H
#define TITLE_H

#include "winpch.h"
#include "display.h"

struct title_t;

byte LunaticTitle(SDLDraw *renderer);
byte WorldPicker(SDLDraw *renderer);
byte MainMenu(SDLDraw *renderer);
void Credits(SDLDraw *renderer);
void SplashScreen(SDLDraw *renderer, const char *fname, int delay, byte sound);
void VictoryText(SDLDraw *renderer);
void HelpScreens(SDLDraw *renderer);
void DemoSplashScreens(SDLDraw *renderer);

void ScanWorldNames(void);
void ReScanWorldNames(void);

byte GameSlotPicker(SDLDraw *renderer, title_t *title);

#endif